package ru.npoperechnyi.globenews;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.squareup.picasso.Picasso;


public class ImageRecyclerViewAdapter extends RecyclerView.Adapter<ImageRecyclerViewAdapter.ImgViewHolder> {

    CountryActivity context;

    public ImageRecyclerViewAdapter(CountryActivity context, PhotoList images) {
        context.images = images;
        this.context = context;
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public ImgViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item, parent, false);
        return new ImgViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ImgViewHolder holder, int position) {
        if (context.images != null && context.images.get(position) != null && holder.imageView != null) {
            Picasso.with(context).load(context.images.get(position).getLargeSquareUrl()).into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return context.images.size();
    }

    public class ImgViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;

        public ImgViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            imageView = (ImageView) itemView.findViewById(R.id.countryImg);
        }

        @Override
        public void onClick(View v) {
            Log.d("View", "clicked" + getItemId());
            try {
                Photo photo = context.images.get((int) getItemId());
                Intent intent = new Intent(context, FullScreen.class);
                intent.putExtra(FullScreen.PREV, ((BitmapDrawable) imageView.getDrawable()).getBitmap())
                        .putExtra(FullScreen.URL, photo.getLargeUrl())
                        .putExtra(FullScreen.COUNTRY, context.name)
                        .putExtra(FullScreen.ACC, CountryActivity.accuracy)
                        .putExtra(FullScreen.ID, photo.getId());
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}