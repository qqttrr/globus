package ru.npoperechnyi.globenews;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.googlecode.flickrjandroid.photos.GeoData;
import com.squareup.picasso.Picasso;

public class FullScreen extends AppCompatActivity {

    static final String PREV = "bitmap";
    static final String URL = "srcURL";
    static final String LATLNG = "latlng";
    static final String COUNTRY = "COUNTRY";
    static final String ACC = "accuracy";
    static final String ID = "id";
    static private String country;
    static private Bundle extras;
    LatLng latlng;
    static private int accuracy;

    TouchImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fullscreen);
        image = (TouchImageView) findViewById(R.id.fullScreenImage);
        configure(getIntent());
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(LATLNG, latlng);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        latlng = savedInstanceState.getParcelable(LATLNG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the options menu from XML
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case R.id.action_onmap: {
                Intent intent = new Intent(this, MapActivity.class);
                Log.d(TAG, "onOptionsItemSelected: " + latlng);
                //intent.putExtra("LAT", latlng.latitude);
                //intent.putExtra("LONG", latlng.longitude);
                intent.putExtra(LATLNG, latlng);
                startActivity(intent);
                return true;
            }

            case R.id.action_save:
                image.buildDrawingCache();
                Bitmap bmap = image.getDrawingCache();
                if (!Util.saveImage(bmap)) {
                    Toast.makeText(this, "I/O Error. Couldn't save Image", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_near: {
                if (extras.getString(COUNTRY) != null)
                    country = extras.getString(COUNTRY);
                if (extras.getInt(ACC) != 0)
                    accuracy = extras.getInt(ACC);
                Intent intent = new Intent(this, CountryActivity.class);
                intent.putExtra(COUNTRY, country);
                intent.putExtra(ACC, accuracy);
                intent.putExtra(LATLNG, latlng);
                startActivity(intent);
                return true;
            }

            case R.id.action_source:
                if (extras.getString(URL) != null) {
                    String[] url = extras.getString(URL).split("_")[0].split("/");
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("http://flickr.com/photo.gne?id=" + url[url.length - 1]));
                    startActivity(intent);
                    return true;
                }
        }
        Log.d(TAG, "onOptionsItemSelected: false");
        return false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        configure(intent);
    }


    private void configure(Intent intent) {
        Picasso.with(this).load(intent.getExtras().getString(URL))
                .placeholder(new BitmapDrawable(getResources(), ((Bitmap) intent.getExtras().getParcelable(PREV))))
                .into(image);
        image.setZoom(1);
        extras = intent.getExtras();
        if (extras.getParcelable(LATLNG) != null) {
            latlng = extras.getParcelable(LATLNG);
        } else if (extras.getString(ID) != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        GeoData data = App.flickr.getGeoInterface().getLocation(extras.getString(ID));
                        latlng = new LatLng(data.getLatitude(), data.getLongitude());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.d(TAG, "run: " + latlng);
                }
            }).start();
        }
    }

    final static String TAG = "FScr";
}
