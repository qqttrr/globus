package ru.npoperechnyi.globenews;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.googlecode.flickrjandroid.photos.GeoData;
import com.googlecode.flickrjandroid.photos.Photo;

public class MarkerHolder implements Parcelable {


    private Marker marker;
    private MarkerOptions markerOptions;
    private String smallUrl;
    private String bigUrl;
    private String id;
    private GeoData geo;
    private Bitmap bmp;
    private LatLng latLng;

    public GeoData getGeo() {
        return geo;
    }

    public MarkerOptions getMarkerOptions() {
        return markerOptions;
    }

    public String getSmallUrl() {
        return smallUrl;
    }

    public String getBigUrl() {
        return bigUrl;
    }

    LatLng getLatLng() {
        if (latLng == null) {
            return latLng = new LatLng(geo.getLatitude(), geo.getLongitude());
        } else return latLng;
    }

    public Bitmap getBmp() {
        return this.bmp;
    }

    public Marker getMarker() {
        return marker;
    }


    public String getPhotoID() {
        return id;
    }

    public MarkerHolder setBitmap(Bitmap bmp) {
        this.bmp = bmp;
        return this;
    }

    public MarkerHolder setMarker(Marker marker) {
        this.marker = marker;
        return this;
    }

    public MarkerHolder(MarkerOptions markerOptions, String id, String smallUrl, String bigUrl, GeoData geo) {
        this.markerOptions = markerOptions;
        this.id = id;
        this.smallUrl = smallUrl;
        this.bigUrl = bigUrl;
        this.geo = geo;
    }


    public MarkerHolder(Photo photo, GeoData geo) {
        this.id = photo.getId();
        this.smallUrl = photo.getSmallSquareUrl();
        this.bigUrl = photo.getLargeUrl();
        this.geo = geo;
    }


    @Override
    public boolean equals(Object o) {
        return (o instanceof MarkerHolder && id.equals(((MarkerHolder) o).getPhotoID()));
    }


    //================================Parcelable methods================================//

    protected MarkerHolder(Parcel in) {
        smallUrl = in.readString();
        bigUrl = in.readString();
        id = in.readString();
        bmp = in.readParcelable(Bitmap.class.getClassLoader());
        markerOptions = in.readParcelable(Marker.class.getClassLoader());
        latLng = in.readParcelable(LatLng.class.getClassLoader());
    }

    public static final Creator<MarkerHolder> CREATOR = new Creator<MarkerHolder>() {
        @Override
        public MarkerHolder createFromParcel(Parcel in) {
            return new MarkerHolder(in);
        }

        @Override
        public MarkerHolder[] newArray(int size) {
            return new MarkerHolder[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(smallUrl);
        dest.writeString(bigUrl);
        dest.writeString(id);
        dest.writeParcelable(bmp, flags);
        dest.writeParcelable(markerOptions, flags);
        dest.writeParcelable(latLng, flags);
    }


}
