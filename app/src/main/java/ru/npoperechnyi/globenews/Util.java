package ru.npoperechnyi.globenews;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.VisibleRegion;
import com.googlecode.flickrjandroid.Flickr;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class Util {

    final private static int SOUTH_WEST = 1;
    final private static int NORTH_WEST = 2;
    final private static int NORTH_EAST = 3;
    final private static int SOUTH_EAST = 4;

    static public Bitmap BitmapFromURL(String photoURL) throws IOException {
        URL url = new URL(photoURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoInput(true);
        conn.connect();
        InputStream input = conn.getInputStream();
        return BitmapFactory.decodeStream(input);
    }


    static public int getAccuracy(float zoom) {
         if (zoom < 5) return Flickr.ACCURACY_COUNTRY;
        else if (zoom < 8) return Flickr.ACCURACY_REGION;
        else if (zoom < 14.5) return Flickr.ACCURACY_CITY;
        else return Flickr.ACCURACY_STREET;
    }


    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static boolean saveImage(Bitmap bmap) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return p1;
    }


    public static Bundle getPos(GoogleMap mMap, int num) {
        Projection proj = mMap.getProjection();
        VisibleRegion reg = proj.getVisibleRegion();
        LatLngBounds bounds = reg.latLngBounds;
        CameraPosition cam = mMap.getCameraPosition();
        switch (num) {
            case SOUTH_WEST:
                return AsyncLoader.setArgs(bounds.southwest, bounds.getCenter(), cam.zoom + 1);
            case NORTH_WEST:
                return AsyncLoader.setArgs(new LatLng(bounds.getCenter().latitude, bounds.southwest.longitude)
                        , new LatLng(bounds.northeast.latitude, bounds.getCenter().longitude), cam.zoom + 1);

            case NORTH_EAST:
                return AsyncLoader.setArgs(bounds.getCenter(), bounds.northeast, cam.zoom + 1);
            case SOUTH_EAST:
                return AsyncLoader.setArgs(new LatLng(bounds.southwest.latitude, bounds.getCenter().longitude)
                        , new LatLng(bounds.getCenter().latitude, bounds.northeast.longitude), cam.zoom + 1);
            default:
                return null;
        }
    }

    static Address proceedLatLng(final LatLng coordinates, Context context) {
        final Geocoder gcd = new Geocoder(context.getApplicationContext(), Locale.US);
        try {
            final List<Address> addr = gcd.getFromLocation(coordinates.latitude, coordinates.longitude, 1);
            if (addr.size() > 0) {
                return addr.get(0);
            }

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(context, "Couldn't proceed Latitude to address", Toast.LENGTH_SHORT).show();
        }
        return null;
    }

}
