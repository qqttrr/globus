package ru.npoperechnyi.globenews;

import android.support.v4.content.AsyncTaskLoader;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.googlecode.flickrjandroid.photos.SearchParameters;


import org.json.JSONException;

import java.io.IOException;

public class AsyncLoader extends AsyncTaskLoader<MarkerHolder> {
    private static final String SW = "south_west";
    private static final String NE = "north_east";
    private static final String ZOOM = "zoom";
    private static final int RESULTS = 30;
    private final LatLng south_west;
    private final LatLng north_east;
    private final double zoom;
    private MarkerHolder lastResult;
    boolean running;
    private int page;
    static MapActivity instanceActivity;
    private PhotoList photoList;

    public AsyncLoader(MapActivity mapActivity, Bundle args) {
        super(mapActivity);
        instanceActivity = mapActivity;
        south_west = args.getParcelable(SW);
        north_east = args.getParcelable(NE);
        zoom = args.getDouble(ZOOM);
        page = 0;
    }


    public static Bundle setArgs(LatLng southwest, LatLng northeast, double zoom) {
        Bundle args = new Bundle();
        args.putParcelable(SW, southwest);
        args.putParcelable(NE, northeast);
        args.putDouble(ZOOM, zoom);
        return args;
    }


    private void initPhotoList() throws IOException, JSONException, FlickrException {
        SearchParameters param = new SearchParameters();
        param.setBBox(south_west.longitude + "", south_west.latitude + "", north_east.longitude + "", north_east.latitude + "");
        param.setAccuracy(Flickr.ACCURACY_REGION);
        param.setSort(SearchParameters.INTERESTINGNESS_DESC);
        param.setHasGeo(true);
        param.setAccuracy(Util.getAccuracy((float) zoom));

        photoList = App.flickr.getPhotosInterface().search(param, RESULTS, 0);
    }


    @Override
    public MarkerHolder loadInBackground() {
        try {

            while (photoList == null|| photoList.size() == 0) {
                initPhotoList();
            }

            for (MarkerHolder marker : instanceActivity.markers) {
                if (marker.getPhotoID().equals(photoList.get(page).getId())) {
                    page++;
                }
            }

            Log.d(TAG, "loadInBackground: " + page + "  " + this);
            if (!running) {
                stopLoading();
            }

            Photo photo = photoList.get(page);

            return new MarkerHolder(photo, App.flickr.getGeoInterface().getLocation(photo.getId())).
                    setBitmap(Util.BitmapFromURL(photo.getSmallSquareUrl()));

        } catch (Exception e) {
            stopLoading();
            e.printStackTrace();
        }
        return null;
    }




    @Override
    protected void onStartLoading() {
        if (lastResult != null) {
            deliverResult(lastResult);
        } else {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        running = false;
    }

    @Override
    public void deliverResult(MarkerHolder data) {
        if (page < RESULTS) {
            page++;
            if (running) {
                forceLoad();
            }
        }
        Log.d(TAG, "deliverResult: " + data);
        lastResult = data;
        super.deliverResult(data);
    }

    private static final String TAG = "asnc";
}
