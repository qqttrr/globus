package ru.npoperechnyi.globenews;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Address;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.Window;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.makeramen.roundedimageview.RoundedDrawable;

import java.util.ArrayList;

public class MapActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<MarkerHolder>,
        OnMapReadyCallback, GoogleMap.OnCameraChangeListener, GoogleMap.OnMarkerClickListener {
    static final String MARKERS = "markers";
    static final int MAXIMUM_MARKERS = 30;
    ArrayList<MarkerHolder> markers;
    static private GoogleMap mMap;
    private Marker mrk;
    LatLng latLng;

    static int lastLoaderID = 1;

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        markers = savedInstanceState.getParcelableArrayList(MARKERS);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the options menu from XML
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSubmitButtonEnabled(true);
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (markers == null) {
            markers = new ArrayList<>();
        }
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnCameraChangeListener(this);
        Log.d(TAG, "onMapReady: " + markers.size());
        try {
            for (MarkerHolder marker : markers) {
                mMap.addMarker(marker.getMarkerOptions());
            }
        } catch (IllegalArgumentException e) {
            markers = new ArrayList<>();
        }

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (mrk != null) mrk.remove();
                Log.d("click", latLng.latitude + " " + latLng.longitude);
                MarkerOptions opt = new MarkerOptions().position(latLng);
                Address addr = Util.proceedLatLng(latLng, MapActivity.this);
                String description = "";
                if (addr != null) {
                    description = addr.getAddressLine(0);
                }
                mrk = mMap.addMarker(opt.title(description));
                Log.d("mr", latLng.latitude + "");
            }
        });
        if (latLng != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
        }

    }


    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        Log.d(TAG, "onCameraChange: " + cameraPosition);
        for (int i = 1; i <= 4; ++i) {
            if (getSupportLoaderManager().getLoader(lastLoaderID) != null) {
                getSupportLoaderManager().getLoader(lastLoaderID).stopLoading();
            }
            getSupportLoaderManager().restartLoader(lastLoaderID, Util.getPos(mMap, i), this);
            lastLoaderID %= 8;
            lastLoaderID++;
        }
    }


    @Override
    public Loader<MarkerHolder> onCreateLoader(int id, Bundle args) {
        if (id > 0 && id < 9) {
            return new AsyncLoader(this, args);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<MarkerHolder> loader, MarkerHolder data) {

        Log.d(TAG, "onLoadFinished: " + data);
        if (mMap == null) {
            return;
        }
        if (data != null && checkForNeighbour(data)) {
            while (markers.size() > MAXIMUM_MARKERS) {
                markers.get(0).getMarker().remove();
                markers.remove(0);
            }

            LatLng pos = new LatLng(data.getGeo().getLatitude(), data.getGeo().getLongitude());
            markers.add(new MarkerHolder((new MarkerOptions().position(pos).
                    icon(BitmapDescriptorFactory.fromBitmap(RoundedDrawable.fromBitmap(data.getBmp())
                            .setBorderWidth(2)
                            .setOval(true)
                            .setScaleType(ImageView.ScaleType.CENTER_CROP)
                            .setBorderColor(Color.WHITE).toBitmap()))),
                    data.getPhotoID(),
                    data.getSmallUrl(),
                    data.getBigUrl(),
                    data.getGeo()));

            markers.get(markers.size() - 1)
                    .setMarker(mMap.addMarker(markers.get(markers.size() - 1).getMarkerOptions()));
        }

    }

    private boolean checkForNeighbour(MarkerHolder data) {
        Projection proj = mMap.getProjection();
        for (MarkerHolder marker : markers) {
            if (marker.equals(data)) return false;
            Point coord1 = proj.toScreenLocation(marker.getLatLng());
            Point coord2 = proj.toScreenLocation(data.getLatLng());
            if (Math.abs(Math.pow((Math.pow(coord1.x - coord2.x, 2) + Math.pow(coord1.y - coord2.y, 2)), 0.5)) < 30)
                return false;
        }
        return true;
    }


    @Override
    public void onLoaderReset(Loader<MarkerHolder> loader) {
        Log.d(TAG, "onLoaderReset: " + loader);

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Intent intent;

        Address addr = Util.proceedLatLng(marker.getPosition(), this);
        String city = "";
        if (addr != null) city = addr.getLocality();
        for (MarkerHolder mrk : markers) {
            if (mrk.getMarker().getPosition().equals(marker.getPosition())) {
                intent = new Intent(this, FullScreen.class);
                intent.putExtra(FullScreen.PREV, mrk.getBmp());
                intent.putExtra(FullScreen.COUNTRY, city);
                intent.putExtra(FullScreen.LATLNG, mrk.getLatLng());
                intent.putExtra(FullScreen.URL, mrk.getBigUrl());
                startActivity(intent);
                return true;
            }
        }
        if (mrk.equals(marker)) {
            intent = new Intent(this, CountryActivity.class);
            intent.putExtra(FullScreen.COUNTRY, marker.getTitle());
            Log.d(TAG, "onMarkerClick: " + mMap.getCameraPosition().zoom);
            intent.putExtra(FullScreen.ACC, Util.getAccuracy(mMap.getCameraPosition().zoom));
            intent.putExtra(FullScreen.LATLNG, marker.getPosition());
            startActivity(intent);
            return true;
        }
        Log.d(TAG, "onMarkerClick: undefined marker " + marker);
        return false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(MARKERS, markers);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);

    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            latLng = Util.getLocationFromAddress(this, query);
            mrk = mMap.addMarker(new MarkerOptions().position(latLng));
        } else {
            if (intent.getExtras() != null) {
                if (intent.getExtras().getParcelable(FullScreen.LATLNG) != null) {
                    latLng = intent.getExtras().getParcelable(FullScreen.LATLNG);
                }
            }
        }
        if (latLng != null && mMap != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
        }
    }

    private static final String TAG = "MAP";
}

