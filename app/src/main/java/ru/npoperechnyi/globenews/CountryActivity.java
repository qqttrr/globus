package ru.npoperechnyi.globenews;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.Window;

import com.google.android.gms.maps.model.LatLng;
import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.googlecode.flickrjandroid.photos.SearchParameters;

import java.util.ArrayList;


public class CountryActivity extends AppCompatActivity {
    static final public String PHOTOS = "PHOTOS";
    static final public String LIST = "LIST";
    private static final String PAGE = "PAGE";

    private Bundle extras;
    String name;
    PhotoList images;
    private ImageRecyclerViewAdapter adapter;
    private GridLayoutManager layoutManager;
    private Parcelable listState;
    static int accuracy;
    private LatLng latLng;
    private AsyncDL dl;
    private int lastPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        extras = getIntent().getExtras();
        if (savedInstanceState != null) {
            dl = (AsyncDL) getLastCustomNonConfigurationInstance();
        }
        configure();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(PHOTOS, images);
        listState = layoutManager.onSaveInstanceState();
        outState.putParcelable(LIST, listState);
        outState.putInt(PAGE, lastPage);
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return dl;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list, menu);
        return true;
    }


    @Override
    @SuppressWarnings("unchecked")
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            ArrayList<Photo> arrayList = (ArrayList<Photo>) savedInstanceState.getSerializable(PHOTOS);
            images = new PhotoList();
            if (arrayList != null) {
                for (Photo photo : arrayList) {
                    images.add(photo);
                }
            }
            adapter.notifyDataSetChanged();
            listState = savedInstanceState.getParcelable(LIST);
            layoutManager.onRestoreInstanceState(listState);
            lastPage = savedInstanceState.getInt(PAGE);
        }
    }

    static class AsyncDL extends AsyncTask<Integer, Void, PhotoList> {
        CountryActivity activity;

        public void attach(CountryActivity activity) {
            this.activity = activity;
        }

        AsyncDL(CountryActivity act) {
            this.activity = act;
        }

        private PhotoList getPhotos(int page) {
            SearchParameters param = new SearchParameters();
            param.setSort(SearchParameters.INTERESTINGNESS_DESC);
            param.setHasGeo(true);
            param.setAccuracy(accuracy);
            param.setLatitude(activity.latLng.latitude + "");
            param.setLongitude(activity.latLng.longitude + "");

            try {
                Log.d("DL", "list");
                PhotoList list = App.flickr.getPhotosInterface().search(param, 60, page);
                Log.d("Dsc", list.size() + "");
                return list;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            activity.lastPage++;
        }

        @Override
        protected PhotoList doInBackground(Integer... params) {
            Log.d("DL", "doinbackground");
            return getPhotos(params[0]);
        }

        @Override
        protected void onPostExecute(PhotoList photos) {
            Log.d("PE", photos.size() + "");
            int cursize = activity.images.size();
            activity.images.addAll(photos);
            activity.adapter.notifyItemRangeInserted(cursize, activity.images.size() - 1);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        dl = null;
        extras = intent.getExtras();
        configure();
    }


    private void configure() {
        if (extras != null) {
            latLng = extras.getParcelable(FullScreen.LATLNG);
            name = extras.getString(FullScreen.COUNTRY);
            accuracy = extras.getInt(FullScreen.ACC);
            if (dl == null) {
                dl = new AsyncDL(this);
                dl.execute(0);
            } else {
                dl.attach(this);
            }
        }
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.Country_Images);

        int rows;
        if (getResources().getConfiguration().orientation == 1) {
            rows = 3;
        } else rows = 5;
        layoutManager = new GridLayoutManager(this, rows, LinearLayoutManager.VERTICAL, false);
        adapter = new ImageRecyclerViewAdapter(this, new PhotoList());
        mRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                // fetch data here
                AsyncDL dl = new AsyncDL(CountryActivity.this);
                dl.execute(lastPage);
            }
        });
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(layoutManager);
    }
}
